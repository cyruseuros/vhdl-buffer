// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Thu Jun 11 16:38:03 2020
// Host        : arch running 64-bit unknown
// Command     : write_verilog -force -mode funcsim
//               /home/uros/sandbox/vivado/buffer/buffer.srcs/sources_1/ip/c_addsub_0/c_addsub_0_sim_netlist.v
// Design      : c_addsub_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu9eg-ffvb1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_addsub_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module c_addsub_0
   (A,
    B,
    CLK,
    CE,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [14:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [14:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [14:0]S;

  wire [14:0]A;
  wire [14:0]B;
  wire CE;
  wire CLK;
  wire [14:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynquplus" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "15" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000000" *) 
  (* c_b_width = "15" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "15" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_addsub_0_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "0" *) 
(* C_A_WIDTH = "15" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "0" *) (* C_B_VALUE = "000000000000000" *) 
(* C_B_WIDTH = "15" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) 
(* C_OUT_WIDTH = "15" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynquplus" *) (* ORIG_REF_NAME = "c_addsub_v12_0_14" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module c_addsub_0_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [14:0]A;
  input [14:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [14:0]S;

  wire \<const0> ;
  wire [14:0]A;
  wire [14:0]B;
  wire CE;
  wire CLK;
  wire [14:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynquplus" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "15" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000000" *) 
  (* c_b_width = "15" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "15" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  c_addsub_0_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
dmmXCzwxW2FLu/vVGpJzoQ/uTl0t/oirVzDN8rGCQ/cshHIr5KZa8hMP1zjDcrW6js/9tSBuCaB1
Ioj63zjqZA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
N9Ijk+dhcsedFOz7GhClRR68iRquy2ZzjVLVhi5GByFuPpr/oGFn021AFcKE54GT1hqizIMvWGS0
qRbWSO/aiWGT8c930WMeayc4xm2b65tzi7UyXSjcZqyZqk5spgPewfSuL0LKD5R4+zccn3yeTyAR
Cpi6LZ2KmpRW5biXvss=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M8NGALCMec7MmW5uPCxfU8HATjWU2XqyPU8phSDe3mtyor4pgfPtg5TJdKOytKhxa+fQwJxytwzI
KPxtYmaRH/KFiGrOvm7jO78NIlt31qN95S7sMYrLxORaZ4bbNMg4RfwEB0haV15qORczgxWEpvBX
6Qukl64ihp4NiBjXt4YYGoDiNMSczgOe3tLn7UWjfPQnsJ8aMxugelO5AciSBxAdohgLMRE3cu6p
gwakO6ytq1vAR8bqHLT8g/Kdsxn72SBHYdpkba0NfEvzzheOlZY7fNuWD48V6QefMjsX1taMkmQH
m38VdXlC6Ocia7H3zT8LvNLtxrpG8zyD+UI+sg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I1BukTJgP0oEpI/mdw6jwrYhUTr7MTzY5G/EvfuPKQfGzYRI1qLG+aEQeclA1P65+tkbstBEIIg8
ZhiouPVaom8KwKZHBX7eLpxvNBcYVFfnJb1ES5wdcph3sgGtaYKSpspp51oYPM6ZD7DmMGdoc/Wg
JVIUuIfRpo8AnEMfkayPkbwuB0VUKpz5BXS50B+5jvgK7cFe2gUp2ckThqzKUjViVB56Swsz+IQe
l7GvtTbuNcSwapfPtNHH0bWSQStfIzPZZm1A2IZ2WCYafRPkj7uibtKNgnKgIZs1197qomgXbb+b
fDx1iikgF8snJsPchukmgxkMSQtLntwbLs6H+w==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UNzBll4hVdQjkp7KJChMHZ9agdKjtTu8+3O75Phz7SpwUZ73Z533+9pCfaH7QI/cwqaVREb20HXT
ji2kU1DV7+Cwbshd08hvUBl23F60ITYS+3rluBLIFX3pzXhjjSu8HQpnxXppbCODvCiWrDLqRU/y
lcFf7B+yp5jK6vEY5xuh8is/oxSPNFwip6GSMqDnE45GU6kU+6n8FTk8pAZUNKnh3j0t6YzcwS3J
wYUhnJpEQYd7+0D/NPjEz0YFqzB8WCh70MxBRJzwdXpuRLiFzplysvw+eHjMPVeU/UPKJWuwWuwc
Bfxw0ThSXZit/SSD+BGhxjbEI9T6rh66FpqbTg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
F7AZy6dB5VKzcudhyRSKWKUbVrSs4vS9jtgDkM6KrVPs3ghP3AF2TeIDyl03EesBxeIQxHqq8thx
uVIGQN5wt92jkzGo61VyUoF2dYHY2dkK9PY4bicayI6rppCS18HnyCC5ODrTMKgOpoj+PEmghCZl
j8+i3NFWPAo6M/MAtVI=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NQpRHEO/CEh2TWVl2zAKLb4TTDP4G4mQHrGzJeErDNbJ2L2B4iz3unsCjzHkoDagHoU9jeHYNzw1
EdgeGwokAwsWnHc0V18iEu5CZPPLrncpORhuc7qe0zJvoIFW4tgNZuIjFZI6bkrWzgxNYlkitGJ7
wQxgR+6ZenldybAjVF7d1R8FQmrEKWQ9ztmGHKMd9PfWD1VsbOoxbNA1tkQ3Suq2M9HDzUONaPQq
yMnGxLE4+4xTZZFVVFZeizNxqQcM1Y6s2MEUyS89U6rdAH95x9zDN8PGrif1SUWhpoz33cYp/IL8
acGyIWDbmuS0X+xsLC8cWcrO/MxKDk8bj12S7g==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
W2ZLxVMM2bO/6hqe8KRsBOYby+akb1JiKHhCv9fhS2DK483JVHKKDFtV5ZylpQSPfpmWVI6nDnNm
XtxOYqhOdd9wAHIVXly/nJQ3BORIgR42ZfKk3tkiYQd75XwTJmWjvIda5LTjKISy58Rg+7/yc6kX
SAKkQWzcaHy0VIrsbeLAK7Rz2vPrBQAwZijqUO1uD9pTa1ID2lBqRKOaN/lex50cPJ7PNmyesOUe
aisZi7+ubKWoKmZJmdUy4nKUk4a0FLkIqdFpmX+Bu5UVgWOF47nrEwh3c1MVRxWa1uvngQGGl026
FTa0G+nc1Q9KslAvMQ+fMbz+FbnTF3u/A9gizA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
h3yxVYfUz6oiYA8NQprAjJlbpD3vVxj6Gv6pXsBd10FX6rFTZrUNzn8DWtGlTj01vPlnbHJnBqkY
5tXu142ls6grHpQ0ZfJZZy/qeOjgMB7UOR+rWE6SsBRV5bQcWKGb2rZgUIDIXmgOXacSscQGKtWR
/6sAHi2aiP9Q+drD5I0PuNnCBnFa1LK3Yc9Ms/55Oyi+UycKzkdRIt43LpJfRxUWUDP5Npw7Cq9P
N6LhGC50Gc/8lXzbzPGXqgDKxgsdrLuj3o2obdqdisHjzQ37iT8lCjA4SumeyIjRYqZqBNYuMny9
BzhjPb4pXrBa1yyeqgqciGDVTyrqbDMfhRqHWQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SRHcYaNGV2+EqR9RxiQYg92JD4xd0h1GmWijOGeaNLCb9MwfWTYg4Xo6D1MLoxevXI2qIXUoop6r
xeRbUUMBxixAEiXwLfdcu8bux1PO9eBpBNPgUPoOwoQYwZl49Wx+b9h8PvGuCmD1HqF6noiiYnAB
oWHwKW1992tGqljsxG8AecEATxWXqgRbYhh5Q4wTkrt/YxcARcjjQauBv/uSZeQSo229MBQo2U4I
IW7UFN26ZkSjYqoJIk63GpMM6ApDTYPu1GgS0O34QsB6ao2KerG8+dR2yeqQaaTHfb/rh+XTP8t7
jc/mKFZD8sYyVjrIu2Qzf5XvNPhm7nVCYFm9KQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11776)
`pragma protect data_block
fC1Puar9AGtjJJikcwOya8Qp+z4VLgE6krVZdoyM3GUwF+iC1XJUaFfGVkf01E3lCljfyGlhpf0v
QdridVI+El+gZk88j/l6D20ggl502/JERvz+Jfl2x1fWOlBBlZqaZ9HNsT9OjcB72dFzDAAT+iPC
xMFhZuXkYqRWoRK43D1UGhPDlqqtGY9+/X6VQpnlHx4GwOMK4R7XV7utcBPIg1T8TT3Fmil1sXQo
3Gq7H7m8IU3G1Od3OG7oxxyE3tcE5xh68ptXzLXXLuGrOtcJFq6VSAsA0lKJEOUvP+8pPYuPDLDi
SHzZT1QsKJ+7f7Y141U0PtEeCIJ+oUoFXZGCi2zYlf7xj/arv9y2ldwUPHqWRUl+oNo/u6d+zGXv
puvb+4L43FwsFXnxwTWMtCVbvUieehZ2y0wI77XgPpuaXQKPauGtx560ixUIQPCptAcsgHuu932c
0ZdxZQKUHHZQYVO+vWbMKputyOz+t7l3w5EIY8e0XXCAU9TxYZcEmDj8prr2gKlCEwWAVRUdgFrJ
EalLxRaKLqcC6F2xKmDBHCgL2jY8IG3P/VjEwwY6qWIPLPUY2gw4HEnHuwhL9LCrlD7AXDWT2Azu
u7a/DOAkaNItNcXYuyJc3RJJgcuevYCF1GaESOGa2hrT06ADlOiwxnKDUa5BCliIEEeARMkaW42d
dA3TKafYjDftPe7vZc+fHnPuVYYRKiOjszcpkAYyar/4l+q7AAWifkIKkXA2yRYzuCIUYGEwHEB4
Txp0MuH+qErWpZN2T5CK4oTOelhysqOKwnSlWQfh6ATWzNhXeH7hE1fl4Ep1So7MUvUu7wCmwLmP
BEpryNoxQVeAfIZLU2NIarHWQo4qolkKuqoc3r6lEG/9iFAhc4Yn0fgF+V5xWJFCaf/QH5nZ6SD/
Mdux9wvZUuXl/5OpWn+SSxzksYzZBrkwwMX/aCjn9hvwNcz7VztBFZfwyPSGuRxjIycvo+V0yOgn
sngISEXzPxeB9GhZLONJIxs3PdVohH0R0mU95R1FRuFFUjMxSRnsi2mH7wyeRxq69XfJXTOqeOgV
octM+3S63Jz2f5p6Pcy80WrESJXM5zEbK0ud13iPR6p6kBvn73ALu6q+qGGrkygFUR2zFMENRD7j
P9WijxKgLy+g0RbHHHeaXODkh1vAIUKZMoK8lD/N2b2E2EBbyRA27Elb89rD6z2B1dU24oG4D/9i
LLDwE1HbpXiNzxk4bmzyIauTVBfu1K+dNDTjnEd24uv/1ikCxcSAcBOYshGjAHKntClYcMnWW/x5
kFHI1aKrC9aZOjEsC1Q+pQ7COB13+phZG0acPPQ0i7Xp+wRjKHvhNrMXW7Tv5daRXXkLM9CGfsOf
tPOjFCdFjT+2PIML9+ohCKF1Y8TZvro3kiK1vMfwYJ+YR0M2lTMvQzRlYC2tQVO66jXUxSS5J0mA
hVAzC2gzDZOC9X+FU4oV3SAp2IfbuygFb0RUByaAYWDk17BrQv2G60D80xqcgeZypEinpp/lK2XR
OvsXTes8nvHn+04dLXOC7yaH640HUngwvkuMzqgDMx987wvXZNG9w0pOcv13I4OkignfBnGu6j97
vSIP7985z3JAnptgeXEh1Nsc6AIPxoJfY5+lGlvEKPf4aFGxlPreria9EZHmGjXZowFDGpcXFTSa
rLuknCxhADVfAbeWANwcWqpLttiG1salaJAT15e2fJiaRRVr9ytqGlNoKV3/PSTSXlT8ez6vhQN0
L6Y+SBml+xAf5ULRnceD9LvKNl6Yskk5jJH2FGLZ/R4L+SXsCL1NNEvOvvBU9qzCWhZBb5pfRr0/
skFD1IxjmaGWlFg94fmuio2v1JMu2lyK3VM1VD5JmtXJycBm+cmk9FP9pXsnO3ipjUKN5ohoopUu
0FOeADyl9gMzTyqGm5e8ijw8sC94oGguAdVkiEQPh2GRcmem9WlIXmvv2wnGsPYxtue5wdM4wqyx
1i+sDHO7B3SE574e3OgYCgqAAFGNPOkkHBuvoPFA5m1AbLocL+TKsXiPs3JV5+QEwj8cIAR1KtJW
2zSxs7qx+cKSAhN8Lq0sErylxcY/99ffn3NnyB+uVifBm/z7DO728bM/Cu5FYjAtCy0eXsD6duV4
pCp6nzSDnWwDyuJzfNiu4e9481NSwO7c5oPZ88ep3UNe77DhlVdrNpfOwc5a1a/9OZptiAkb9CIM
Z92vwUUkEiCjj09BIQ4q7UcFaz3+EcjaEy064ZIYF+ZRm5d0Cx7oD3UnYEKZezDZ2R0iZNhPBG3M
fKpK907gB/zilaEqJUkIdTnoTfTUHsYSEf4GXZl+IWDKS7ny6zH/4tIFAbWG99CnEDIJ57e36326
kpS2rwTqiYKqEc9dLvRJz5M++2iHiM7BK+rl+as4cX4QR+X7/bI4SdbLTbV2YhnolsERTCgnQIH7
/zUl0BzuiE/FMAtI8Dk0buJedhr95BG7JNl2drLHS//uF/AdOr10HP05OMe7KFI6HOENj42Zrz26
ZQnXJB3QN7ZuLq2xripRAoXhSgNN3YtxQlUj+Tv9XUyGQE8Onrqgq0cBdmj9JpydJokpdW1Z/bRq
gZ6EYVDxa5dmXoE54l3EH+Tpr07NtjM1j709dTNfF1JJCTRzSCC6G5BKbyO908Rgcv/XUbXo8B/t
a4CeV1IYdjVZ2XHPTEsIGuyjb00CcW5eoS+wcC7CBu9+Fgvy/sGiL9UAvPmRWwC4yy7I7TRkNtjF
YoC+qEuHDrXdpaiimfPg6eRcoAJtsEVjjfq4USMrBGjcuZzYg8//hDGlCDvGrIj/Kgk3bg7V4et+
26nyvm/Ygjn5K2/XB/QfknGoNHRaztAYwmVrSTN/eiKqR2urV1JP7CDPzEEkCatHWOdu+hMq1jbL
BtojIsLrCa057yqTOdyZ7uvHKZT1tlPvSM8ixcRjBHIgpLxGZh8IlbTEuGbbKqmrLxh2rk3jxa+p
42PeHJXdRleTNc1vaEL78eSrLcN6rjVZSJyZsPPPf/DSjUAGoIEyE+yEDigyRKQSRKoXeuVJ8BCo
385+VmzPYjmBsePTxglIMxpFN6H/b8pRKKhqIJI49PNV5Mbm+Xd3KGR53pSUm42DqjhpntgHaRNO
gyJWBJII7W04UWQ50685ixR9tVr2dQtK45vq9bug3JDUAJRYnkqQmjbTubdJIa+TD4fFPL9qi7WD
8MvWhmDX3JT3ravSN+EVmGEguRNtxljs8epuFduBgdqf/CiC3zKSafJYv99kiVHkAtbO+mSgKBQX
xekQkOevI4+5Iy4crSGG2JcJYsEtRwGywxjoJdzZqGC9GQAd5Wbbb/UQjr+e2d17IMaxcBaZ2AV7
q9Pa6s8/GupmWEjx4oRqzfr3ORJA1Rv5UBuDtjb70wqnAWm+qbwia8GmwxsM4+RdwMJlC9kktj1S
CWZBxiHsudPZHfy9CknJ+qW5jUqDOcdTZMg33e+k2PlpbsxUj3DAY2Tz3SGZcoJdxWQ38xKN8cXG
H8iQCc9e49OEC02dAL/ckVhCFkNXzirML1gq0HE8kfsVvNGeuWRwAOD/tgvz7nx7ob9VyRCdWFIN
91+SeyUirHds3A53D4HddwiLJDPBDqU9TxuFI552XvF8K67dw2AQFBBUhjaFuR/xdQ8Xvfoihy4S
pSlpt8L563dQL5cVDNrGq+RtXiLYaAj1TAzT4JVgEFevdYy9xHZyTrG7X5UIcF/qTbPObE9Ey/NP
yNhJhHtd0x6UgrSwUG5wQig55fFJWPuC94hIyGar7usUGSs55GD9L7udY2FfA/vMGBzxsP7PWL1g
Jql2wvMweKCJGG+0gKxy3aoL9nZDjfx/WJO7kpAqahINKFGCJk0drAZvdWGCcXU/4Qj7XAzKjr0z
8V8FiD9endqa9hf4dIMfOCHkBZGrzuzQ4wlI+0iENPtpwfSWC5YzE8tYNaUxwtRHJZfAEauoJMUg
IGAbxd+4ByUDcP8DPRx3Pijg30erlXNMueNqjhoW6LOC8Lab4To+k0bKp2CNwZDtG+YX5fPEpkqm
xiT21QsZCGPYmH4SKlFPR8APqeHzd8Spjn53W5Q4cFvyA/oIYIs9GzdSuPqtBd1KqxLJmc84Q53v
TiNbhDyiv9JHZxLF51P2xr4PlfCKuwR4W0aDNbfdGXW8R+ju+DP4jDlXy0ZLVYGNcJSKuO+f2DGU
BNLTUqI8aH2k9qp8URfWUov3CyLC8k1olcE1CcRohfsN/o/ggV4NzLEm5thKuobXaPEpM21H63Cn
4l3VVGLyI0O9j1p+KRRrVfUOBIDexT58q7THkvG/KvxCwfTQ+hH1+r2I1gXMNbXbNjdTl9Mp8+2z
Px+KZvdi5j6M4+w7xnGPeacADieWACLUH5jT1SAz3csP29q0uBB1oafpxdb2glpUeqxACEFeHEgN
AKxRsLbAntDWoCsr53DUt0qXtE959bd2CgFKBFAQH6qLTnGB7IjkPFNNh5klikWvEWXOV+g0bWBM
Yt+DY0u1d6NVrLrZBwtIkr5MHYP4lWAQeAoal4v/hd96Vultl36aR4yyuy3b/bDL4TGTUI2Bz+Ce
RthzZiC+PJcMBnwSRE9Ehk4RkbwtXf0dbzHtnogTAQy6I75wZDAStE3xM5dp94QNPYMrqWpLEPer
/ksz4YfaJsiQykAqEW+W/EsE94whuNxOrj+b63Mrkt+xbBN7YjWeMOoFbsMVKyXGeKG2gggup/TJ
YGzUi9QOuWPDcQ3zNDiNK91KNVotLOm0BJed7Stq4rof/N/utZrjSqmW1REvQQb6felYW+opR0RU
sDesF/CQyiS3xKLXMAYqPxAf/vVgVGHDrCeW/afhZYkFnB/HUM6pcmYW+KMQs6bcMVOdxeuvZbkp
vXqUajNT3eHU+UUwbS07htCzWzvRjNjYEs6tqh55CoKKAbJ9V5sZ0pOZ6w+ozrQ/G+8+uSH2zYZs
WelXUvMcuKrNNfiR/ILOFyOgKN9VaSjwNCY6PFAgCT5pCMzqcxnQPYbsMSkyV/TK2wNbSd1E9EPd
VcskIkegPMVkQet3uEErc6kmzLJW2Uvuy46OOK4gn+5WwbhvWycI8EMlOh7IDHsCJIK1luEw/u4X
vQtV4pMjJoUDkMN8jiBlk7afWL1v9fTws5MJevL6OsAGO1R/r4m04mkmptmAGGmv/rWmux9K8V6j
t3Q+iQmsoJmQDey/gWpd2DqS78O5B5tGxdIZPAdL2OLvDRaG1k6ztek/E6Ddex+rz4vyOiDZloSI
OCHqXvxPwe+WZ4qw0HgsCpLpfG/03YMEdX+ux79rHfXmSZ043ONZnLcpdkp5CDJ2p9zmt03uMvcY
RltJRBqGi1s5Tl+X8wL+Ga4v49xCc2rMXKJ8jAvOIGaMK75S32XvbSAaJ/+iscv1gxfoC5y7VSTY
kmgh8q412EOxGB4r3An/6QlkAUuDnMd4BuRqaWpw+y3adZDeiOUpdI9XGSIkev6QVTzvfR1oMagx
11ux+Abwfa7NqlEsXx7MZZ77yMFhXCpv+XmHwZ0yazef8sxKU5HZeShV266o8Dwj7khbnfIgAHre
aD691RMTO4kiDbfrusVyPf6OqqbQ9m0M6ZoWtNyfN6b+ZWRWovG+AtgnziGnl/XT0u2bVbe/ReTs
Y0dkudAQPLcxilmVG/ycabRmiyE8cEoExswP/lArgYM3dSL1kMWiaBSxSCZelzh1KzGnc21p/r/Q
11vrFj5XX44irHWMLZ9o6tRXBxldEkO4iR+YBSFN03/hjoyl9k+vHFejqVkLtwGohXdj3YUg8RMz
3Z6oRBJSlGpp4kutHgxDFWgDymaoUodKpZiC+7K55KqPbXPW9cLCWzN8ZRo0+qZhOszygghfR/pw
QK5uORnkEC+luvH9R48J8RjQNIwZf0G7TLm/ZYeW4mrG/7pCee7GpLFzRrswkMD9i3v45c7vcWmJ
mAI3OgrWkDG9KKRR8qRlyP54XNPIgxNnnfiMWslj2yDG1/1SkRZFj4gpvOIVJq6uHxIKSYaYmcql
TXhB5MYuv210q7df/NA5IOE0BlO6DhiAuFTLmr94L10di0TlGeuntf32+NMLs9pHYzDqayiIajKV
z9Jkt/wdcm43flpQnmbGdMHjwJlwVf279SZPROmhMvUqvBVNe0lbxFi9qkiw4srPFVIBzQw76bK6
n+W01DvS1gDES6GlN2Bpmooo4KAi2XMSq0wbW6bs9qUqVxJt4+cr7GuxOPG5zGXCOehuPKm0RECi
w8t7pJW7ZGEfX/QYEmGZyLk87dt+f5J5jE1Oso4sPcZezTOm26lNV+luAe1IoBEbrfCWWlWrJpq2
KcRb3F+EeG9+pJDLb8nVQXnjEsa5mZALGwZ2gEFd9ppV/8SgCUpbOH54ep2BD7T+PyCAFNhVy8NN
hzEWp0fVeiEL1Iwc6Z7EI4U/bFBMoarfMqFaIva1XGqrRf4xZlyF41SoQSmW6eUYIBgoa0eN29LB
qVoXAAkEJGcHmqqsYwBpzf8XaKqLCwHoJQRVc0SVeZ4QxIFa/tGncPvk6uWh0zveG5UslwOR3T5H
EKkN4mx8sG/2xbh9tyYlOMAoznUnWo/S6KdtfO8qSUm5ykfgs13NspRq/zS20bZYDBMiH8Gv0N49
5BWvwvYcwhSuwgLkNBUx3OBVKRFLQrmwD7l2Kh7kiCQcjBSAX81JAxirdU1N5iyw7764x1HepH0E
z6IK1G/KXF5Q8sEijUcFwikyn2JVL1WLjzkGDTVZ6tCwlb8QYDj0XvZxh0MYYBq4KdYmaD6IG7sK
lBCgYPRfzxGuVotKW39UojJZUmVtO68GaR9K2hC5IC77IQ+8Ai8mVsB7D3hE9AvJ7PQXdTJ91Y1k
MjrOMvcCTn0bjeSNX/+x0aAPH/bh3tEgSEsxPNXoFvGy6Gr2mvd+7eR6KAF9FHksLkJG8t1lAMUP
AnfJvHJDrLD5UD5iOTKK9ZqJ6QPpyChpzMiH7j4KDzGFsNBC/uUNlXg84XhrxFmFJ3vGOi5n/4vQ
jKPTmfexA66dbZIPVkTLZGetU7pc8ZTY/1/e83mBJc/ogzGF5eBUBcMoe1XD/WeDLV3OvGS4W/i4
3cnlxU48QhsvRfCw6dA1wAG5ZX0ZRbE+ckcXzRWSlPEjU6pg3liKTbZrbV6djXKvnGfuqLxNBdTH
1Zdlku/EqNJ+EUOJogxc5FwfjnXQvMAgfGGZApZJve64HsamwfY5qc0B56dnA9gRhFLscbqGgPtC
sjD6w5K94AWAwtGy/fv0dlZJr2xxY7nQrgTdBGnRMZXabejUNycLCkrlUHN/9npmrmT0zOe7QG8F
CLy1Mx1Sw5wfUV4uEYge6wLHjdXdh4VaI6RlOma77WnToXzhqXT4jeecjJmMmI2EFZWBEhSn6byc
gPoZVirdk8ilduAwxBerB0VPi71M0u5mBLLgVMKNP5k5Bk1KeVOkSwTm035FWn7AoTOmwIxS3lK3
RmJvzDxWEk0BNIzuajvnr36rcIiEn5Xc3wlrG+B1SwfzESoNkldQN68EffYI8iS5NZp3l8nOSut1
bZuds2GowBtAfxmbh+shAtb0ox1eG5eOjuTHpfY+6L9VQIHGu/oELdb9Hk5hvNfuvR6vDjdqvOz6
BhsNLl7keQwSZh92PEtA3KfV78kWN02KK9DfepM+iH9lomMzJ8uXYF/bAfFAG8FsfAGRl3tWXAwD
5bpN35gmPm+gAi98ibXgV2sXLKdgrQ4aMJ0zbtxLrBGngdQtg4N09ErxAKkleIkjs+KuBjvZ758G
z9iY8irUhOU/HV+mhZ/uX0O4RJOzYlGX0heO8SrvC4s8N1cI2/V6mL9DPyBNb1Gr/fm8LyWPztYR
QWkY+MeaICRCYs/Y5+Uwf4XldnjQevprd572WjaztspQpAb8sLh4Ww/6Y/dPjOiOBm64+xN0NKUE
34x5PFwQ1lHx5qedmvFPeVQeGRtCi0sSMBIm/Az1gzPlzsMZh77AlBPqZbaxNQmFtb4Ut9OMtiHO
F/Yz9qOrz7qwL8+9q5inXkar5L8S74QzXdYvTY3hBSshA34gJpcThKwjtdmsUOl6b+j0jABREqXj
vtUC0TvX+b4lhXfI2ibi4mxPtIOwMXMOA+2jdKUCT7I3cXRvBCrFIc7K2E/SikmshiT9q2/L7lDX
DGfAmqO5jLlw4ymTggX1bHsLvRAWNW9xnGHwPtkJmVwcJ9/R0B9SS9kB7R6kKeyZGkFqjJiNoBtr
LQc5m76CQ+caqCpMh+UMwHwB4OKh1CAuFpatuxylD3gVjRfmBUtKFAdek5fk0B2Vb4+ITrUmg6u2
aUlzr6EK1Z0HVGONnQrIBWcdOXVvkGo+4LljqOV/y07nCpZfezAUbthl03Yx24f5sfTgwaRys3RU
I13pc+IZBcc6lLS/D1Fid3fGRckuj9R6WbEkVMKY529njfd4wJxCQM+oR9cDHgaq7OcxP/9aXjm3
fBAwm7yaRBmNECgOv5ZyzBj5eJ7mhhbt8pOxOpaTcLRxyh/dKhlcgXp+G4JQOurv4TxJmpKzeEWz
7rfPOONXfjLadnZPglOFrRk0BH0x8SwWjg8t81lYUcwBmRK4vyHmUD0j/XvD+RDgyPohK7lDLQO/
zpKwhKdBP4v8wotXTVYLgZtQIgv/t4NFrHhMGs6F15gNZdulNbjy1o6L62VxKRqhf8uGzYkkytgz
dnW2Xbzcis7KhgKyljluLmp3YHiodyhFPVq/prMgao9vuUe5HKyTGzAEFg4ae0WwlwF8cM+lgA/q
1tLarXBIqaYyF/zBhVOpwhuzjyqay8h4y7RiuAixJQs9CTzfGj10svJKtwAqOmG5UebPke6fdKZI
zOwgtHWxWv3a+O1jB7Ytj+cS8Jcff5Qwd1V2M2xMivzdPmIUxiaFgucaYTjKaqozJ0TjiBoaiyGW
LGAV8HJa/G4tmhz75qYxav/ZToEplhTch4yalnWXRjjCjOYK20vNBjaIdyW+NcR/hFG64lXo0MlG
xzjO8lLx45YgHjyk3frOPn0vHSbXlIvy6LTq6xWJapBhNDZw1jCKtgAqDCtbR5RTiG6cEfKljTlt
0ePOrjb/GAH7s/3H1/0rcJ0S8oJ9V2dmfiv9nfd7Dl8u1pEt0yK0gigjnxA2iclOFBc4PphNoZgl
WhT7DONIWrictXC+SAriGj6ZIKKrMI19gyUB6Pmf5WwkZys3aW/sIQ31fvj1Hs0OzmuQji137uKu
w2MTNFfCNnXqHudYgPjVCkWtM83D7LAwK3Nn/9WVXcQd9xKgQ2GWeTv/0p6bvZHhlAPQVQdvwHiS
xzMRs4VqXIb4mnHxP/gu32960Jb43E1auyXmzuQRrWExKgXWBAn5XfEpPdbGrew38zv8fxwHOTiU
bIy69P7CPR7WUPz6HLJ/MKsMNKPzxEo09+1a7PS8YILNo5hqOihSbDzvIclC89eTtZpmuDrv29In
lBRwj4OOI0jjOEqlDZm8Ia4pXhz0WGN5Lv6aeOX0c5mOQFleBAHCXuF3keTKDI5L78oluJbPR6qZ
6ukSTy1vtEXqcoed1B6SLsC7dfW05GLyiJ43gwyLzctUk6MxvzLnyvo6rVRrEPtRsoYCB2sdyhPk
AXoqc1qqeo9zlGT/DyCouCRIoyz8ktt4EmT40VpGLmVziqZX7aQbenMAzc5yhjSEU5rFB+JVij9g
G/QIFo0thhM/FQHVi39aGx9gRCsMJbkHTpIQ/UYxUnfzSEOButnl/xnQgynE8GMVfZ29PT8gvxb+
VuOfXSl3XOMN92HoNeqfJ/4fNsDF1HkYfmtSPcPP8yYjZMocb3s8krsIJR/6FJiRTd2lvlqdVW04
t6/Pp1FHHOK35t7ZPTzCtXOK+Uh7glV2nRl3xSuShVKzAZlnTKIS5k1lh+R4wIWmThOi2TTKmdft
c+rqFOsJFvIqPmHwHk4DD/ndXZax9yXayz5SvB+0VqQtBnLso7Y9jLSTVGwnQpH7O5UYEhadsiHs
TO9i43vL15994ELKIGdDZgE8T+dazV8c/g39wIrFe8NwZBHfq2InzDMxKB/yRbMIDlw7G4/3kJ7+
WAVLE6rGXyCq99dk9Hch7A/SXcoLbmdhmp5dKcD6Nz3og7cgMStByduBtjbKQXb/olXu+pR7keLQ
aS82vgWbDmDluzQD9Y0SA8z6tPCyGYIbunb/g8goD38jVQa+MyC/HOVz0CIFaEVJfHBfwrT4eAiU
HAFoHmMBOMfC0q2tH+iZMJvnq3UaumZv4mGUkizNAzW1lo6+tVJCPLTrSW6Dhg57ZdGk9sjA9UgR
ukdkG1Zsp1sAc541p0llNdxGKevPNHlAjTHymUfsfLPtPNr9Raj7PSwgmG36S2cDQcd22S6Q9UQS
S8nMehJmAUiFfG09YGevKixwiBSSEvV+2XUEFYqZEzsM43HDcQJ7WAIthc4nGQtN6ndfEEUbYCXq
CIKAlohVH3p91JqWrW/cLwCi5blYxr+pOCpylj0PZkU3hiy9f4LLVMttl3ZJW3B+lqoXhOxNmFee
9jkzRWixhqf99A6nU91Ec5JxGws9vGA3jw01MjL5DfXlqZ+CWAUjd1CTQbN1KZnCl6WuWAgucGti
q1DTI+n6nc/NDXi2sv/qLgw+es5zpOGw0VkpaM3krT2xoJlLIHArxaGzzlHc6yaue2Whbe4qAZqh
CAajIxBY4hdcX+TRfU/LHIQtbUs96VL2sIIBk557S9IPmu5KYOPHeC0+27E4pRIPAOqijeFjkdKq
2Pk/c/h2SRYp4Vqvljn9WsNmN5DBEvqUyF9TQuB/6/6I7zD4DFiQyyLpn2BnHiM7Cufm/p9IKBih
T0VzT0+wbzNYDoVsuOOMF5CLaJaJtkuUsb+5E4YrvXM7ChcDuT3ihiz+KaesPDnI2JsYnYO3guBK
GL1YGEVWwznM6zecf8QeYp6Vqc8mhTVRaP3bwkgm4c6avichgbAYzD5fZdrNofyt2L5Gs6eh+syj
E1yyGdhu9i8XC6PmdfTneSY25taU/7PjFVUmtdQRycg+uE380dCH851rgeBYtgBfhKXng5f0dWyr
Z8JJq6PXICD76wseig6h3gBM9uFu+edXpyGmVwqhKDKJDzBuhw/fmkwXNpxPdlN88aFIoSyNq8rE
PjTzRKpU0RCA0XJ1qtwPNp/QNSs5JLDoomhy/XA/LjJVLzKGzwoqjPnpNCaq3qo+w0KJnBhY51Zg
X3BYV8x3k35xK1XXeyQQpS2gnSX3Ltx09z9yEBb/CVOjPzYljlpcUV//WWeDO5dloETJkDrqkOA8
dQMUFz1hAS5HuFquTy62DCWMmBSMV1R0HYVGKRpXzsvPBvKod72o+wxyLIIe1U46+TNIIB87a0VG
LAIcRQdWLJKC4alzyyDu3xF3YCqY5pd9rcNXAGAYOqdOblDEckdcGj4dWcCYrIO2PgWAHqoXeOEJ
40+bcx3JSd62zzKgEwNmnUS2IgOWwf3VDey9A+6IhdweOLnxCEpKjvNtsIvTQlZtiz1UsK/gDIze
1MyTgOeRg/3ZL6FP7pVOHyFYWTudTphcb4CPYYb6QPW6rg4YALqd/stjIHEVTd2DvNfUHqAyVXel
CprrUZPTUanPpxRtoT3HZNXnahxParjZ6eOBpi7kQCqOwcthroDXn5Ip3QFgvoRmAIqIgrfuDTe3
W00envuZRsh22AcffXQjL2oQYrPgMONSbc75wegRSUmdA1hnN6GNZYBcZjPdXHjYCGyehHQ9aXnP
juZlTYdxxtaQdnnRxWp44kIKf8AaVDvicgt3lkrPbAEcDGaGGI5yL4BDRP8yiCrOMw/EN6XjpMFK
HzoFzzZdhlocMevtEPqo00ucg+VzxiMZo0mWQXiEVbTqVklw2vh6VwVytv5s3ZH8OsscZFCLcHSh
myU+eikvdfqdL2CJnAe2yfZ/RIYvZ6eZltuwvs9cXY3MheymNi7ZLYBSGUsfQCp94vDwFUH6boXu
SFnrWN5doZeEa4FSzuw9XB+bvqoaGvq9TDVK9N4YNasPX4grvFAYFj3bWYMqqUHPzeliRUF5sP+a
9n7NWrSXwA+WRa2nndBNE6bhGcVPHSxSHASTJIU+jkbvSACQ/YtbamXNewqiRtmfuDPsM0phYO82
YdHEg0gfjGXsEt1MKDlMZ7tvUdoR7B79UNxVoGHg2ftHvlQrlI+wKEky54Z1bLAX+9LUg4nU+8C2
MDbSFet7kYD2g4OdK1HpymXrIPA2CFnDq8jMx3tHfMCJHOyA4Y6UKKbsAmlqNO53+rBEhcLkR6s/
pYIWV4RbLe/qSTRLbg3z37JGgMoeUw7EQptClWVWVxLq0go1Twa0fF12zupFO/lJUczpgB5fWcVG
G3Upg/p8lCKvH2SL+rxHDxlsWV6lL4vVAvQ6JTaE/E4IRgVGxuSHzGaHE4Imss4aP0nPFtekkTvR
reQ1lrRkjRecX6SxJp2kpUvSlvEZy2KpTvAKQtVVxRlm1VzvC4dhkjgNjccSYIwqllJzjNt+JEvE
umIoDG/b8ZV26oD1PqaTfoxkt6Wtck22fLyb0GDLFRR2+1ShDoVYyq3i3AjhO5x322Bajqjkdi2J
iaD2QtSeuW5D9/j/w+SFt+M+BJaDNaccpn4vam9oNl/e+bkJNhyS8UsPsh9Rg3Lcp3KYKozSS48j
5/wTHpz5Fv3qPVlHoOdmxfA3NUtNX1vmBwj2w/iGlJfemj+hRJQbuN5EQUW2EIUVfY64sFVmfMlh
ukZfYFj50FL5c2jv2r919exC0nEgwCmOI3cpfHU08s5Bw+cUSJIe8fAq0A+UgoF225M0+rUtJqNq
vjYOwscCDfNVsxIPXoqLFkBhlUnMQaD8wT/sDOs5yuT7Ic3/1YC1H1c6zIz2sFsxsChUG5pjcd8Z
kpWToFPrI6sAuHFw4XuoSZqhGcWB2oXJplQb0WPWjWgg1ybPKGtBODfraiQY9ZsHyOz3PDeZVPuT
3hGVks3UP2yqRoS66y31nxLXFa1i4y4up5gOJ4of2S6CERT3Xu8tDyFqB7l5/43mWqVPUuOPHW9H
4DJc/sg/vqiZIz7xL3FGD7glDM3wWi76KB0UeJgXtgKSUnB8ZsiqNVP8/RC1W/iRqnVhW7G3UNV9
2zeTOUBNYYwFKRkRRPn0+oMZhDgiB/N9+wCklr/N9FH18WmE25k/Ubt+TrPatkGSpubuTbXHtg8W
mFAho8D6fv2FmplW4yR9rfBR/GmJzdpg4CdkCrVBhd4fCmogo5HnbCEA70EjjKZhNIS2ScrO/JSO
FFB7H/iDTV5oUH6v/wAm4sC2oQtdcvC/dyCnLcjnVFfBNUcOzGvsfYkppjbPl4ZCnvm2QcsajIl+
zuR52zt75zcJjbi6V8/HlUCNdS1Sq/rg9LxdmB2GVEfRF8xcq0hD3tnB9Mthru9taGybZzORmM1w
jr9qoFSV0aZ+JSVocTdiHh/HYMx2j7IRcPRFg5RjcxADR9mIsgKuEMU05qR9KGsb1xVCGD2PWClb
T1x9xInhlhh+FOLomPC+rGZvibbFWrcOpqwhetrVUj99efX+YwKLMbhADpDnxFBuQOzCReT7PK9G
iLohumsNd2ZGGCAvGLY2AfV+VHRs6HQm5aaGQtbTlXu3wf+6W/QVZe99h6o1ipGnbnShptvSwktr
tJAealvpiPvxSm1s5ASH5Lc98cfLFmVseXzjdRqELaAxokY2Pz8J3UkpoPN1SkyWndSkgiutYXkF
ahjt5gY9pwvTQUmqwfHAMJ+53q+YHS4qZth3hUVp07RCNpJtJ/NxIROmgrO/g8di34Uq5cvLsiy5
7Y2fKPzAIJm1TLV5p3KjxDvJ6tc3rx74AW4VTFTvxzN+rCm9WugpvWCFWkxc2ZdxsgOSoz5HZaWw
YmjoYmlLimFQ0t5Ac+GsANIQqD3rwArDzHGo0aPH5MAwDj6/3lUe0D6T1k7bERXxfRpvioJ+b+qm
gUj8TMB4Wx3wfVBFto17OM88L3QqxW6jCIWZ+8qQaW/n3d4v9ucx7aEGGGYfp4WWWtr9w/LOwwTa
LTR1jGexZzwonDU4CIWjH3+et5VYIW3R91Ukp6mfdGoPLRv5NwjNLQGdol5I9INjNVsNPf3nu/CM
zDZOhsWb1uHu9o2SYTrvy3uxeWN2yDXPz0To3nonVnIiLEJuQIk63IIm4pshw0Cq7IqNU5xRERl/
YnWHEP8HngodujGYoMAHgee8krZB/SHlR3liM/vt1ZgReYqnr3Mee2rYqogyxKlVL2U8y/QHh52k
B5PU9zSreUhrHsBjbirLhDlUXkj8Eh/zH5rG/uUt1wsGiQhRTy6BV5ISfUinvGz6g+CLQrSEs8gv
0LpyinqpXteJVfxcAWSUsy8IiriS6UOLgsad9DDJtlmycY2BgfJytTUUz5HsvcW0LvX62d9VUtDW
qkmXcqEdUJq3QswaJCRmVuhHR0uOiFCAlZkcynHhXvm4/V3+6y7O8TgM0WXWabnkh23Pe/596lrH
7lNMvCageaioIhJ/KSeVBrTXRw0OEVKXesU22p+rcvWUYu4IezpX+RPHanhn7c828P9Er34fJOXs
GjBgiVlMpqja7x0wkd8m488SfyDdn8nxQGIh6cW1UUZdtOWkhS9cdoboaexRFVnV+Q7uzfbzifHc
cXohWC64Vue6vSDpsOgnE3ObfLvk/TJd7tqCXNvxHSr7lRteCD4Th+gXm8jNOwDObPCOJTn1pKvN
2VExV1JcqFOk2p2fK8dcyzlmvBTwiaqzalqFxlGHcsDMmcW9UKd1ALRtE1HXvfM47grwxvbnBj+r
xGNyfbENMoU6luP+vpOcjNImFvcABMMrA/tJyWcyU0OEP5pgAPpiJXWSGarVME24u3N9taMJAqIO
wJ2o89LxjXDr02PrJ9x4MA7n4ASp4XlNrxY7HIdy2eyG0hCOhcw8QkCoNX1th0UfuodDb4J+kPDk
VRCO6mWzq9Ib86eEDL0TL4KsGPR7fTXSz1kcJBz2da/9FZ4p96YWkbcGvY5FD8j9pzYX+I+FuuLK
xCkNK8kIfDLArAIzMcjmfYdTTxsOUP5V/fsZRz6PXXku6NN4VamtxX2Rrt8ILU457MOOMawDR+oz
/yDCzS3s2kOqF86vWn6i7ufE1HyYv/09fjs31AxCjNqzZNQee8HqjZAmpYE8YnGdfXSW/SJtI3p5
3bK+rRfYEjfCY52Qe3D5xKHeA3kurY71RlOuMINWCQibY0ERzn3wdoiKqGgNO8CbaKJnaoQZrI5U
Rk/Oi1lZS9CXRUnDasXIgNUpzxzhFMfONyJfIvEDzdSOnerJRGeI1d5xLmTs6uQ4d/1fxbqH41Zi
ViU5Te27U7Y7iaMn/hUG6HOEiH4o/NmE0E7kS5GfuaKyNO5RJDCt5HjpGPNLouXdqfbYEatybqer
8NDesRu2p8DJ+/T6uJ1o5o5IZzvYCEis6aa6rMeZ8johdaaIYYZR87tJFYO/UM1Lw6hDVszx/WnK
uyfUnv+lxEuEDHua/nh/A6YMbN0T+d5o7ZnkrOyEVn4RZIGJOuuM9xLwtY9SumPZxEdHIWeoy4bN
4kAOhX2VRpRuNzzXxeLZ5rJr7skYuzKogoe0kkwUt4+CMLGhh+ZVZLmK1bkyWA+tyMxVLw2jDL6k
eni6oOZxW3Z4+isbKuvkl2cXlgUI0Yd78MVbNgr1shNfvAycfTFQ+dBWldGU83kk2N2LmXUgQVp9
jlIxpSaOJhTv8q5nPPvEuWFV9rZgBtPxxcS6XTDZG0jcVw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
