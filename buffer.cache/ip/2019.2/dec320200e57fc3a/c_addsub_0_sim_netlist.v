// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Thu Jun 11 16:38:03 2020
// Host        : arch running 64-bit unknown
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_addsub_0_sim_netlist.v
// Design      : c_addsub_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu9eg-ffvb1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_addsub_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    B,
    CLK,
    CE,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [14:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [14:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [14:0]S;

  wire [14:0]A;
  wire [14:0]B;
  wire CE;
  wire CLK;
  wire [14:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynquplus" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "15" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000000" *) 
  (* c_b_width = "15" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "15" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "0" *) 
(* C_A_WIDTH = "15" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "0" *) (* C_B_VALUE = "000000000000000" *) 
(* C_B_WIDTH = "15" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "1" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) 
(* C_OUT_WIDTH = "15" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynquplus" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [14:0]A;
  input [14:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [14:0]S;

  wire \<const0> ;
  wire [14:0]A;
  wire [14:0]B;
  wire CE;
  wire CLK;
  wire [14:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynquplus" *) 
  (* c_a_type = "0" *) 
  (* c_a_width = "15" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "0" *) 
  (* c_b_value = "000000000000000" *) 
  (* c_b_width = "15" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "15" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(CE),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
dmmXCzwxW2FLu/vVGpJzoQ/uTl0t/oirVzDN8rGCQ/cshHIr5KZa8hMP1zjDcrW6js/9tSBuCaB1
Ioj63zjqZA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
N9Ijk+dhcsedFOz7GhClRR68iRquy2ZzjVLVhi5GByFuPpr/oGFn021AFcKE54GT1hqizIMvWGS0
qRbWSO/aiWGT8c930WMeayc4xm2b65tzi7UyXSjcZqyZqk5spgPewfSuL0LKD5R4+zccn3yeTyAR
Cpi6LZ2KmpRW5biXvss=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M8NGALCMec7MmW5uPCxfU8HATjWU2XqyPU8phSDe3mtyor4pgfPtg5TJdKOytKhxa+fQwJxytwzI
KPxtYmaRH/KFiGrOvm7jO78NIlt31qN95S7sMYrLxORaZ4bbNMg4RfwEB0haV15qORczgxWEpvBX
6Qukl64ihp4NiBjXt4YYGoDiNMSczgOe3tLn7UWjfPQnsJ8aMxugelO5AciSBxAdohgLMRE3cu6p
gwakO6ytq1vAR8bqHLT8g/Kdsxn72SBHYdpkba0NfEvzzheOlZY7fNuWD48V6QefMjsX1taMkmQH
m38VdXlC6Ocia7H3zT8LvNLtxrpG8zyD+UI+sg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I1BukTJgP0oEpI/mdw6jwrYhUTr7MTzY5G/EvfuPKQfGzYRI1qLG+aEQeclA1P65+tkbstBEIIg8
ZhiouPVaom8KwKZHBX7eLpxvNBcYVFfnJb1ES5wdcph3sgGtaYKSpspp51oYPM6ZD7DmMGdoc/Wg
JVIUuIfRpo8AnEMfkayPkbwuB0VUKpz5BXS50B+5jvgK7cFe2gUp2ckThqzKUjViVB56Swsz+IQe
l7GvtTbuNcSwapfPtNHH0bWSQStfIzPZZm1A2IZ2WCYafRPkj7uibtKNgnKgIZs1197qomgXbb+b
fDx1iikgF8snJsPchukmgxkMSQtLntwbLs6H+w==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UNzBll4hVdQjkp7KJChMHZ9agdKjtTu8+3O75Phz7SpwUZ73Z533+9pCfaH7QI/cwqaVREb20HXT
ji2kU1DV7+Cwbshd08hvUBl23F60ITYS+3rluBLIFX3pzXhjjSu8HQpnxXppbCODvCiWrDLqRU/y
lcFf7B+yp5jK6vEY5xuh8is/oxSPNFwip6GSMqDnE45GU6kU+6n8FTk8pAZUNKnh3j0t6YzcwS3J
wYUhnJpEQYd7+0D/NPjEz0YFqzB8WCh70MxBRJzwdXpuRLiFzplysvw+eHjMPVeU/UPKJWuwWuwc
Bfxw0ThSXZit/SSD+BGhxjbEI9T6rh66FpqbTg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
F7AZy6dB5VKzcudhyRSKWKUbVrSs4vS9jtgDkM6KrVPs3ghP3AF2TeIDyl03EesBxeIQxHqq8thx
uVIGQN5wt92jkzGo61VyUoF2dYHY2dkK9PY4bicayI6rppCS18HnyCC5ODrTMKgOpoj+PEmghCZl
j8+i3NFWPAo6M/MAtVI=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NQpRHEO/CEh2TWVl2zAKLb4TTDP4G4mQHrGzJeErDNbJ2L2B4iz3unsCjzHkoDagHoU9jeHYNzw1
EdgeGwokAwsWnHc0V18iEu5CZPPLrncpORhuc7qe0zJvoIFW4tgNZuIjFZI6bkrWzgxNYlkitGJ7
wQxgR+6ZenldybAjVF7d1R8FQmrEKWQ9ztmGHKMd9PfWD1VsbOoxbNA1tkQ3Suq2M9HDzUONaPQq
yMnGxLE4+4xTZZFVVFZeizNxqQcM1Y6s2MEUyS89U6rdAH95x9zDN8PGrif1SUWhpoz33cYp/IL8
acGyIWDbmuS0X+xsLC8cWcrO/MxKDk8bj12S7g==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
W2ZLxVMM2bO/6hqe8KRsBOYby+akb1JiKHhCv9fhS2DK483JVHKKDFtV5ZylpQSPfpmWVI6nDnNm
XtxOYqhOdd9wAHIVXly/nJQ3BORIgR42ZfKk3tkiYQd75XwTJmWjvIda5LTjKISy58Rg+7/yc6kX
SAKkQWzcaHy0VIrsbeLAK7Rz2vPrBQAwZijqUO1uD9pTa1ID2lBqRKOaN/lex50cPJ7PNmyesOUe
aisZi7+ubKWoKmZJmdUy4nKUk4a0FLkIqdFpmX+Bu5UVgWOF47nrEwh3c1MVRxWa1uvngQGGl026
FTa0G+nc1Q9KslAvMQ+fMbz+FbnTF3u/A9gizA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Q+34dIEZB9p7emZZSKZnO5NDAZiQ7iDEQwIlms2UsyizC/qpxlWf/sdKEnXuEnCrgI7o+XPi8t90
cr1zxzCU6jrqI8GFhaCcwGcmAeNVoKABOYYrZ98EePJayCE4D/6g9JV88gphsgOFimRcvuWn/nqP
6PAiFqV+RBcMeGp9YouS+v4x9ijj4sDOvCl1dCml4yB3I2fGPP9JGyrXtJAW9CMoy4/dpXrsWMbo
bYB8Iw7ANCDQJIU9df+213YRVGtiNJKAkeVgVkxLAD2k3gIzaZ83aYEge7djYLsEgigXXJNPebic
CTCVGA8HBoYANDqKuBdL+WN4qiAwNyAYqUWWWw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
5kdqwCRxcVNTHqdFe8vz4FCs5wwQnaGp8FJe8tPctyc4MOffynmxsa32H4s1eIYXOOF2XWFBEsL5
a7pkId1zM5ML5F9s5nnHpB9onUwM9DNxOlPUSwQmauaSDF2Hpa/GJ/NgoA0+04ZmnAs2t7Uo7ciT
93mPs21lEEZep8OEpz58xzsQWAaKyTro5ZOTtM9DyqK1VpLgULLCz9t1Gl3+2+4wSdMSStQqXny5
RsT10WW/F3m5sgGlmIp2LFbhqnnBjMaE4zWo0qXXYAWAvvK5zrWp2AwxCfp+yL7BFKH/GxfdPwUb
Y6UbgOkVUerRWLHZUWi0nZaGmOLu9JMAn5iN6Q==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11808)
`pragma protect data_block
kXioHO+7zNdN+V+5G6SlWJXc/r1pD0USGtzV+Sy12g1FHPG3u5bQy2Ih6b3wde8vl1nDkiXDW8mS
9RslKqkj2aBuJI0+Fadth9qXqGlT5XbLsIHJNzDN1IW0bDeFYdXVJfLoLvs98x/R9S7RglkpNDpR
93QCKcrKflwnDRE3+BmpL+BqEAVgiKptA9tFVYYScumawq1V8Ce75jEMS1v/Zc5Jdg7q40q4z8cY
OhpZ+ekGu6c8I4L4bHmp4rUhzHdg2gAJaJQPyl2Lo/xImBLoOgEE1jo10w8jpBs9ybHXgPFkaYA6
HKlajDqIP+ntRQwNfTDrqVXX/fvZ7m/oFZE0BfL1WZnaHhayI72P/OPCxbVmfKQOcUbaXLkz9lJK
Tf1d+hzaSTxVN19BqYc57E/SvnAZLa2DH8ikSARm3roardvcfEUHODBIIGIQXqiCg6kypUFcmaMm
xpbiOa9VVPfa6NMbKkOQbB0iu0MEfRnO5eOc3vjGNCNl5EkE3+2X6akWnSO8ohGIzQWoG/LbpBjH
vJE3aPkwGSFVkBdYv/v1YVkSaBfClyX2k2OCI5D5XvSn7spS3vaTMeeZqfItTkMGU2+GffBmU6fX
8AtB6Rg92ywjZ6xJojVMd2I0OYfGtVhiJJxU165baRfjkgdXAnyaYqT+5HbhAO25Ng1AnVgI9G21
XQ+AZ8aUS2gmpTXbqtxuqgxAWTFAqevMY1lnSlPQeTgSipZ+uPVK9eC412O7nK7heF0lHo7GbJ0+
QNjt+SHP/k0fhY/RV4gd+ffcdnT4KCqQErJXryzF68EE22Xg9S+JBfTjWaDDoU/h7mTe48akGHYf
9uqUC6/7osH1IwaTMW6ZZ7G9sBK1faADvcXUipHKSw1HGzcGOwtXmFyAM6DbXpPS25+U/JeAVq2S
0t8SMlCSw5GsdYgoCqcSE5kWZQoONj+LHHkFZ05qnRqjyGDfdBKrfVfuvu2cTDoJCU7AixSo5r5U
5cYQlmUvdw2t47CPQ3oJJw+z3D2ct8GcUAP9LfTd2at5aIbAj5DJ1+WaieqUoup6QPo5cAENyR+g
JegCe6VmjB+WbuHEIvfPOCcvlWd/ACPWJcRNyF03eudgWCV5mRisl7G9lD6608YGyVkKxFtnX/2j
jJUv0ncddVxPpBrXjz5h7fuhvT9ESNMfHfqvAJbdigqltnia1jLh+FjeOtP+sg0807TXPx/n4KaO
zllugAlM0+Su282Qjp+ovmzBKC4wo0Fd+keITwCgatUfR8wfncVad6uuikgvzJF+5fpjkQ8OQaqF
t649Turx5pnDzqutZNq/fDANLHLM6pmjmrGTlGhcgbHMRmnCGm4BpcErnlmSzlybcTWbgT1ptJ/M
NcQrwp97AOgplVHUWrNgazVZOWFclpekMVBg4sovd3nWIohMQf2SWdhfdpo3e4nWlfLtbjcpdd0t
jFiRNa6xrzu1dvEugWXbCaK4ecghayymKyewgyN3VTrHGLp7q6ORgZXaMdXW4Vr8P0gI5AVAjWMP
EdBqHVTXnFQk63XYNj5ZeGgLjLEWwbDZc1lnELLsbzPG7S1cdNhuvKbch2Y9UKAC2hQPv0NSZn8L
fKY7Iye1HK4VSLxG6iZE/UF8WuX0Yai3kT9osiYrN/iV5jPUDGyO//3LNPMpcelJ13Z2jgEKst6+
D790W7MA9CFz6EJBeSh6lnZ/rAvsrbdg/7hzN+dnBhDsPh3O9KAyE7V6DhAo3b8uHuWOJhc8rTeP
fdO+5oh+zvazqpH6NDxu0fFmC7CNb3bl2C99T13Y+44Q1D+Dl2SJjfAmvBIR+ksziCCUQgmsN2QI
Zr7y5DJvwSli53kyjtXnVUXeFclZHd+6E69teKpxd291euM/CAeiBn41ct8q70hI+cqEBBS20hU8
S2fCvD4Z5uozZyzjHibhiv/+6ICslwqvTv0R2bWROtOKybatp1e/ZhDfkPS6YZaydd6zbDGwr9Aj
YSPBh93lGeeft76O83l93ffJk+4Nq+vAXQYaF50GGQ05C3rAadzOCjonp1xUwl8xtS9oxUQO+321
Qvhy/qgkKK7IRASBUu6pOemJgCL6aI+Uo1d4XWXKmQD/zgmeQwZyGaK+XZ1mBq6iNkoVtXIhowhj
/2hMr2g2UqE2PKPLpixL8g8JmENMvT8pziunoQUP4L8waUYORpzXcCWp+zz9Yt8xEpfgyp/JJzbQ
zeXcz+tjV1tqDIWTe1W94zWuZk0kRP1ParATkLSowcWowpYkN6uPCF8ePxOoudneNYInyDt8MLIj
P358eO/8oHcStCs/8ZsfF3VB3NBuS2tfx+vmdLzp21SvdYDHAYgzTQah1U+SFXFZxLM9/logqZfk
HjNeQKyk0p27+QteXgg6fr1z6N3Z/L8LLqdso419i9b994tqjfKZuApzxWIXi8P0pIbQI8FJWYup
GzOeNCusI1LbO7gEkfNGu1EImyvGVWG/Xpv3BypYjvO5mCsZPWUjKMB8FPx/geM6Kd9msA2Pb+Ri
hHSlmvmtamXmZ7LviQ8X77n5gOQbF6lymsLaOTx3cG4pqmYNaPCjImLd0+GefXB5BBg5ekVdhj97
UAKpQRlY+xd0DW9+aGkZPhEb9zlgCzxr9L/dI+tpldPzRFIbFSJmUg2Aj9CWRN1qPL5GSQhpfLii
rqYURywqRFsO/PgCwmXE9tkA2x1odAZfnzLi/4SFNs/FAeCupNl2y5VupaxMCa4jodZPCt7KDVL8
6L+m43MPaRxGqWIsl58Y4d+P8ozOjcGqjBxzPVwHBJdUe+Xe7LBeCnvM5k85ACw95bCjVJ6zUI/p
C10z/vpeaMnpte00FW10/UfdI165XtmCqF+OvSm/9nZNYVI4OcCnPVBxIISsC/nn83s7d6DSNCjo
Y1GiKSI0pJIMeItDCz4E1xAHHUCWV2XBPGHuvE3U3Bd/y+SyaFxLMIyjV2rrtLxVQ68fsR6Ke37N
mjNF89cQztM2QVW9B63uTWc9tPvIE8PqCAHafHaArD03fMufaS4XawsNd3shuYUbM/qBwnRRGafj
mrS+rmWinjvCzSMV69GoYcw7nNNpggUTSQ2cjZjBu6BIEnxjt833TURC/lI4uh2yXfU1gkQCjy+u
fR7jNVsIC09jg9T9GWuS2btKAYxV3vECC6QTQneIfxJ1xoNDj5pzV/XC6yag6aalbJ8uABUNu8yg
SC1iLGrdGtqPOuBS3LDZIvnOfk5q5Bux/cpItawjYLdeTamRvRrq4VtZ+NAbLzYh0dmmE9yfNp3y
0G/0F3IHcD6QvWuxq55U5DRUhq+yeA2OsPIoEp4fmI49JWHdNojHQo1tnEbaj5D2ui4yg+aqnpl7
r5/V5Zm0Rg6S85fpQQKMkl8cFuhg5VL4ew5hrZfSxkLcYpZrxC5CYII5hDXm0Y2P+FKV2ho1CBLw
pGxOgC4fruDw9jYKdb23w5wegdMtTbZScHBkuNjj9EIK0hkqYt3FJHoSlgcpKl1WJe0gVb4m149i
WPxSBi6baEswj7gi4RfznsRSIlgg/GIjDvkeHXjz5HWE851+uT2i+KotyI3fOGnCJ1V1h/6pn01E
uppu0EQLJuy1w3pn9vTDhdBAYMevyzNLofkteGpDttzC1nmKUt0yAiS+9wSSmxRVuQOjPFnEVUVX
qKvtEJ0wjSC3pBv6d/W7eUyj1em9kIvu9GhdAcztLVzz9MrIOOhtGD9r3QXCIrGXqw+5/V6xd0Y2
q3GfzCcp0OSMbPftdGqSmhkRUvLgHyAlIcW5YlwmFFKIduGR8x7657Vnl/Ygb6pV+aka196lzM4r
lm9TnXOSKD0MbpYZNmA8IpSJ8xYOezTldLMqvMI55XvhJWIGbBpFgd+dYLBuPjj8OmaQk8CUVV5A
OdaMxbw/j4cXMJ4AxUzOd0chvErBEkeyrVqQUuiaB+GFH01sF16Dnv2wwlucDZXkI8WCEatPXTRU
jczxY46krvlZddSyiKG8rpDz6MHQMytwe3H2J35JzLPf7sa/FXJiRoxcgQuUpXtI9d0t1WsUyQJI
FpjskTTxXy8qYTzQ9tgF75zQ/L4Lb8vU0TvKgpH0oxoQpTuYvnE9Bc9eNlxAs2DqSV9RebnGI+Yz
mzWcYgqYA3108Ts0JmMta9ntEAybnLPxNHwLrusnaAjuzo9luqI+IkTgKiubsIJcoVN5mE9WCcjI
5ym5NfbVL85lh+JPYnlcu9uqzd8px4foGiQB1Q0LJJhqLUiY/UCVsRf4dYHXlKENLAeyWGByATW/
JdAJx8yx3MYI+fsHkPs4KfI9vn48R5ILT+JthIC1DbdrQAig1c+Jrsva3LJPCpLCrfPZEu10Y/v0
A1WBKCubLglA/uduzlJNJnpzet0zEMOQxrgFBGeBnjv06vE9yYT68mJRAOMsZUPObOOeh2DXJ1AS
oiTeQmM4EtRQm8WwAuuoeXBezGe3Ab+LFizJlKWU/K6UlahJ6TQc2lP2Qr+WYJo+QrMkFdeAms5y
GD8QpeBycLTAncI7IUIgIygcT1AZ8ScbeRWRAKPF0qo0JyPsNVC0yZmGCVfUxjkCG3xT/B8mex9s
v2io5OLSMZYUl5p90KNBVGUUPMe6/uS5rdcQlOaZEMITJPeVgshPBZuYu3b0CGFJWIbPYf067YFT
JowXHV+P4Mp+CT+kKBS/MzgpZ3USnPjUnx4HQaeGFUdQMSNnU/dclWXyPVmjlXfXdZKGI/xaCtoR
k2KrxBX0mhddAtB6sKoxF30hfBSwvMNftrJ4R3l4js1Bxt/+N5im4IgNCDyNsSLYY5tMTWtylOjW
fDE64+6LO/HopQjdZznnbQ8SJKvGB74zaFRPAXzQlMYJR43NCvE+64gkwZ10R4yKWUNt0L/pb361
5AhlediHxhyS7nhcjBDLvET5tiwH0r+y63ZZLAsIgg4NvHWGycaxxGh+K7tEPjzAAFQ+hg6wc2cx
ZanN1IeNMOQ57lGo/gA6gjuNU+mXQ8IE9P7JNwxv20ELk8dalNm+bBinSZgtVwaLP6ARkyiHrawd
sO81SuKIQt9IgEyVx4mwb98RisQQeMWtGYPw+lQYupQOpKOuPF7OZy7jfTPSvwWogCY6dsOVTojz
8d1TzCbh+HH/uaDCpEu6V1d93LXCm4JsKc2T3kRzaT+yt1nM2XuZYji9Kn9vReK4MGZDN3pi7WCW
gWsnaO6kO0uLBDK6BJmJEiNrzNytPEdoLpNQeZR0IPIusvL+gbL4k/iDBA7D1OEGBdJgV96iRrJA
V+0G6r1YfzNoj7VpNZj9sAPPCiG3dqan2vZ0mrdCxjl+1Mcd1+KrVhaUBFGpxy31K8hZEbaLowod
kx4bd6QSXSewnfCbLo8lAUyD9Dnl61LcegqfYAdgJv7jdV+rrsIYRlXv7MYrqRqBo3em4Gb1iNmK
RSeGz2ExTBbId8nOGVNONC5YkEfG0jrsm830uxYJ9j7eQNFNMulGWa6c3oD4ygDDF7QZLhumxve4
vfIXosDiuevpFoXAE/zKv3yU/t9oziwlR/hgaUz0KsU/VrarWbkJP0ZsPfbNxfaPEwD60Cp+gQMw
HYlJtZI0XU1jcjAAWJfrL5g/ww7bLH4VxYae4w9w1DI637PC1Vd7//msFqOgH7XK/1WdVwB3n47n
vCMt8AhebSIjhq51UpnzBI/9qXBBXNv2PlxDwQujKDAakjVTq+F5oD1uy4qhc+VMNxY62/xqwxra
wecymynjH75FWH04rSiDO6SjRBcbHDI2FCU+GlqtT+3PvdYmk9/ouCP9b6QWHDcVljJKAAcvzCUG
xmG7qgd4txzzqbp8L/to7OM+P4YK0ZcVUpSbWQVN6eMCFh5L/sPAP6Wy7FwDy/c72hT6y4omyIsR
dcjxmouA1wnA09odoz7IH7oEE1VLNeMZ7nCWngHYUTftjNQO+VBxKR17Yh3fKUbLN6ULFd/c8KE6
7aQ4n98SF1wzNV3j8/nmhNZ9JIIUZ1ygrBnct9K/vapRrkWmIGS/FQxVTHp3GoQFzU0Op7lwehvQ
chOhZ8cSB3mW2/LMgy4FZhCgt1bQuV47QBtfscQaqZAgPxs5d+UOaFMuwn0geHATuRNTmhYnJ+jf
w/pZxEhdmZ7Mh/SPEbDLfAzM+kFafFnBWSk4ldsra7IJwd6t8KOCCfTc6TtvRLXARQjgowBgWu1b
QqxWf7g1cg97kdaclNw42ARX1mtPywlzIeTzGvwNtuFNUoMUn/0SNoaVPJ75+4DeNxSx0NDqkn6U
ZA5NC7F8NU4ovhH5JjmkebRudFHF5+G7IN0SAdKE+fcf8reNhyReYBWKcji1pVKIGxBziDrRNs72
+WUxuBGE9CJJQLjQBLsAs0HYu9PnuQsRQG2xYDlclv5WGnHX6lKN4cZhKyFbYesCELpJZIKM5Dm5
Dbl9hg6aBg2mSWn4Dfk59FbV1i3TeCOjRNlBPUQT1YtUFVCfd9O5X97zB5lWyAEo9KXW/B/6n/BQ
tJru+V5HQ6awSt+RfVDYH9Or7kqvt0Kg1U30zQmwGJ28vl1Q4WXHC08Gt5r5oV0mpcmdpJHR4H0F
Jp3Mi9bCmrEYl2Dxts+NZ6C3w7uf5+C1rXIxS71fT1eGyMEEvXZ4wLMaGRZIJ8YJX5ACZRgae83P
RSDmkLSQW4QKaJAL2cuWIS5L/D+1v9cDXRBRNfPoR566PR5BliwK5e5rElHRHgbsit1nL5mzWCEM
W4JqI0Habd2hUyK8Hqbcz4nJjCWdofuCfX1Xj2KT0gFpn1yqAMuGRR9GGQdWUonh36fBA8CYa3/w
tk71G2mJqwGkCSl2rKiUqkVMM1jKTZkA5/lE9dNUKJveUPDAyEPpJvy30d91lrpkgWl4kf9c/PLk
uPiADK2hhUZK2RyChBzAtcEnS6rw+rjskpOfKCvQ5PV8Q/FGEhW5HsjDiRkKnxVvC+qrem32Vrbb
exs1cQiU/Tggd6WcAkfX6y65FOEmKDF//H2+FvXm1eNyDQwLn3WI3tSFYJAmYf0jg+3bIH+tfD1b
bHtlhU4U2uSGydCrGLLt4ZbvPTuTDRdB9foZmuohF744uVfQBr1csJsyL7/Bc5VLXBRHlw68LS8Q
7eY0/y08v+Edm504QkpVn+zmnMJ2msGKeAgRZ71FoNrG2nU5WFukakOye51EZS33zoysUmMvCSeT
YrGx4qgYqHvQLmMhTED6uSWraNizvjiKMIKoqILtQmqES2L4/A/JZ6TlPLOCfw2GEAGr1apMKjp0
MCr6Rh1daqpHgeRtMn56lMr2BFZ/F1SCOxUlMwUWPcs8pAL4xicyHV1LVCEVhyuOzLiR+eu8MPtR
bO4VHaOX/kkfuu5YoImRx8Hos/cBTwKZwgjn1Hhuw8nm/vboWe/1Q8Sn9CuDw4WzCNf1d28iPPXh
vcTVrg6n54kLeZLkEYcSQXJdXTtQk3Lrdc/TOfReB8+23L38Khd/R+56Dx98uoR8t1zxsEjf6SMV
xzNYodHcdoWO8R3ATEf1MbQ7jofhZC5HVo/XQHnhPtspnEwiuimclSJzbD0Dcye3LSMS8I2VW4DT
WuVejuYOhsvNBOk0yc/M2//L7Q/cxXT3/Cxat6gNKGwfCfGqRaqO5CeZWM1aEizYFaJ7h3xC/1oR
nk/Ji4Gl/G0ZRon7pwvItW2t/kzGg2o1/elHmFP2awA1j9n8MozGB2S3lEYQ6GGrh+HFAcuxhL8m
skVe/BL0Lr02foJCWTfM0BZaLUXj8S4X8yK/+c13ZAy23cqLgD72T33YePiBWRGanp2Bn3CsLAWI
Q1rh69Psf6yk8UZzuhHqUddavvJ3Jfkjr945imgVGwojHLyK+hFhgnZcwdvnpCph8W9U0EO2Q2F7
lgI6eOSDxdsA/Uo9BzMKAspFrEffMtgItbduZw/u6TsjWGw3y0cSRKferJd4Ieya7P3WVtpxAnj+
2FHl4sxJrj0C573wkrxEJBuTkEcXzMZ7twLpb0lalf6Ezmia6/ScyovvYZO2L5Jd+TZiaV9c3ZUO
ogG+Y+NGM/rbovxZpzJqLDceuWtQzx7hnDFNK57xFXAowNqhhS35p0/LP9HVgSpIZU21nkyzyQ6+
S+2DPblyjMW9NkSMh06eiEoYVMl0TypajIZNYHbhBOZixTqEYAdpisd4zsPPo/xsBQW00gdqyEEe
UytWDRDAeGHsIY6yHkdH4Kc8q4ujJZbdC9FpK498PY97UfzQRTltXJ8B6ow3piAYS3j7ugefAoKX
XNekl1SpY0MAP8lVR6AfbHDnN4LQZPid3CYqfvfpNRqOG5HxY4xZ6fzHPsUs3LBR7lGjU45RdEy4
qH2jTp8yUkTd1eXZAsHC9gfYAY4RfrfTUA5x3gRRzBiSvV36V7/lYnrMDKiYsAWr6pF2S7M1EDM9
BqPFrwlXAQPwU2WiaAx126682yjfmLYGLeX0Q41rchfiA3w6+5e/sQkXNwMcFqY8oHGy8I9dbD32
/DmzoGCWVy2yFy8NqjCoPvdhd70FARn/igNgyLyasWVyPmmBuKiuFBr85i0FKaIl9WRv7jWBcVYe
RcfFgGbPsPjVSafz5nhrfR16DAp7jWQoxWbMeniWz0qdCfbD8HV5Fz0Cv/EdT7vNNq1aObtknfGU
xupSykxMpIrS7dO1kSl/QbTi9xr0g2LfuE4u90TJF+DGyQ8T+E66XCK39G3izoRGTYTC6iarc89Q
N6eWGj6pV2Ieb58URybrQbMzPQrm/dsmRVnfy8pVe4j1Y5dONfxsS5tgWWa/vK+tirlI8ymzlVs+
okP2vAEaVsT8Eny0al0ilkbeDfzthQ6j/D1//9YuO0dsBDjIet0OqAiL7CexhRuDBA13+vnFvLmb
owhHObdYnXTWBP4dkh3FrfkaP+lKmhHhP1osftgE1+J8G8Ym0hDx7aPYU0Yf/bYGWennLkqlfWck
5kuZqFiCQZBeQnongxKUMmaHSePTMjOcITDJPcwA2DOujvi0rqq+6GVHAd3FxmyBxvXewLWKQw0U
L3RTgfMakWIKt27oQnPWd35Ly+EKkQ5XuDhfqCnqOxyiKk9arHXhnvPbw0NLlnmAhPc88sWpYOB9
La3rxnN7lZRbyDtlmNXuLmA5srwD3eu3IAWZoXIgD57vX1z+2gHLXrRPcLMLOrJeFUtJ/eNt8vuf
vgEnvCoZCwHrkJIvrLVdDrb5kvbwUPlbBfgZ8bJCSZj4qjVVMFU5WpFy5i5/Gy4cjEXVLs3sQvNm
HwLa8DpEoh+qXU2heqQ40YCaZ0QJt0HdO5mxSvm3MghbsFdEORE3XavWdoQpZkKTcHcYwuNu3rdV
gueJ88PJmI1KcwMoGGwo94hEPQxl9NyOUNiicRUEUsKvX6OBVpBaiUwqqZq+orBXb6zZiwxb7bDG
xdOe/HpX+CV9YiBCXKkxBBjtMMuBQ4Vh/Zxn+5h97OkXJVLPdINS47kIz0aaiONDT/vToYzg3Snp
ShOOjmEFI36Zhd2AO8DnAQhFhQKVchj0061cK8jMQUseXdaVtEMZNpZfVYq9VtSaOBbj37dMnLQH
jycUnPnvq32dtCjKN6grKe5GS38cahgcRJ5hja9RQMv2bZbpqgCMgkxG/fukujfL9N5ZzgkWLdR7
OQ9wycCOXac5rvYdTbqCEEp2bTqyrVqK1NZ+i4EP7+GwQrJB7BFwLkOZAzbya4RjlFzDJXefQTyx
6BtQf6rFgldrc9oIMNjjyW8nFgVH2eEk7kSYbvGyOyLtTV0h/1wesbK0oeoiAXYX+u15TNezZjrR
hP/CooDcwbik8RGsJFpY34JO2An0uB4U0qPZh8MAXSgFOujlCZ/b8y0ngYVw/J0dqV54uxUcmg7j
x8gWAvSFz7XFTBwAx9yBKviH/CgWcyaI71XkMzPDOmeAU+To7Rdb4/IRph0lIPqAg/NlYkbomWyj
IAK5JtdUQenxQMdz43LNYA3f2CnUV63W2i144qF+dUp16M9jvhQcvZzeCRWBVs3YiwGBZTm29lyA
EIXFn7Bwfh3dnXFm7XY9+rx/1dpS8AEcCmiI6jmuHsCXT6gkAfEHlKOR1Eel9ToS+yXwXuvs8iTH
Vv+xTwcuVxsn+BNBqfAGoU2HkJC1N+v8Ust0QNN25wdQxX+u+BxJFA/nrEhZy5D6v08CZV9PZAmH
rJ+5czJMupixN7VJva/+p0toMxV1J4COpeRRV03nn5pSj+Hl8F7I2l5H6ydH0v7XLReZ5S1HOA2G
wOni+/VpHUm2+PAr70PzokKVH2nIFtHNivoadJQxrUsUkhtbYTUz0S72bBcLkZQB0cJBGgIeB7IV
Qonm1nRHGBH1X61FGWxiNqFApLSSdN7jbfCSnH9PrYzZVd3LvnbpUutIQFAA+RqUcqdtjNaKt1I2
4YiYd9+xHNc6Jgl6QRThx2Dn7btixuJHtSfzKLEEkTOnXOeHD7n72dWoMXVRrpB8oO/fA1bTu5Zn
4ihGHHQzk8XqB489znQtaikhUu9CunDBF1OXX4oOBo8WGn/2ZE1p613/8y3Xlw8rT56XYqb29nGL
IP8+onyG0Qvmohqq4OGr7HUb8km3ycYE++wQG9+aIAARuduYpWMU+Httx3ICbbQhxYR6df4/ww5I
v3SzH1StBE408gLGSmiHkChFWqXCk4dMmc1bs0Uz2zkWhyH5fhfe02TUStiGaL9Uwf/3OD9q7m+U
84gAEcR56u/jKMRptfN6e6KOYa7ZqmcqhOFIsL1KzDFv6tzTeDP/i+/pGAbEHZXdpRV+uKq1lb/7
tmhiLnjxl2fIzTP8I2MfG3GTyLDD4DAPWn0iOibOexy1FgaVGbd2h6OChPqBwK2ifNCdmOW+0Ori
EI8+eDVK+2iEyfSwhl4T9fdNIyHu5XdpFt/e0wDqjlEozqiDG6+ed3y+ufhMBRLzUkY0971LaUDE
ThV5U1h2PyXzD7cDgYmzBOzTsHKSefP+PKjJJ4x5BERt7xMU08AQ/7jGqR22wprVimCxldkHXmyR
GNl7GoZ5tn1/2xFV/4Xd6Kp2/uFC+Gdl+qZmbPkvaGtw8K1XH7FjE/PT2rrbWiw9rrXDpNqrLG1t
6YMy1koMJQ0YmC82giA7pQmGqzeHv5sMxZsndmAtTaK/tHywviS/WUZbbMHkwaFj4SIsrxwA+rhc
Hq/+6RYKuOy7hPiC5/J9XxGDl6PbgWH5NFqrRsrq/SMMEu4yvoUbPBVfEI+bn8G0IzBrb3XZikLd
jk7pwafCe8o5GigbCIbPZjWmh6OSBOeiew3/fyBIhVaoxvc17ysvTH51xyaGCvK1EHqLc3U3CrG+
6jN+vZf7jMOSz6ve4uCSc3l58RJmTRO9KSXo5WKpUx6ZJ1rzkZWJhxOG4MYDW4xUPw6WyTp5iIsR
TPIk2hJKS0/E+OI3TsV/JNTA0AFqGwP5e2Ka3unvTBtHmEcoHppmguYmA3KG6C27fNcfZNeYPVwu
kxLoOEJ1JUvMr/65Fea24rFLwW/NDG4wA+qG35lyJoIpjsNClBjTq/F/Pn5Ae65Im0YxpjDyH+v4
qK/6xIB+9vSov7nLNDS36bvHXaj9e+mi9jBvDbF8AHGE11aZiPR4bf3UCzXy9zBsizeK7Ble7Crz
dtMefENsRobMWE3lE7u7mklyx3TF35F9byhnWwWOJhJLN/d+o8oNYGoDy63zdc1mZ7NUnwiAbYSv
RwCqh64qRFN+d/nOIVHKj8RNay9VDmWsKfVqwHLBsr5jylyOnPJjCRWvhgsSVK6lx9uc97BVBScf
wa+sPJOamxbb3iIWHuTxj6xMlXQMYLuN1eFNCUXsI1ENyhQb9/KoOBCB0gUcop+odUsMC2fNqPnc
w/BjHXZqg+2Ix4cU1rkG+2ERjJmYz06Id4Stv9j2vhIk00VdK1uum3yrJkl5e6ym96Ah59J+ObZu
yfyijHn9txC54EH7+ju3y5CJw0iTd/Zs/P1IEpOcRlNelz3zBEVo7D7ogloC+k26UZ8cfNF+1bHF
T4qcz45JLbkZj9/ggkd8GVvryDx6tYPM3HWlWcVinDoUsQOqbIAHjVFKe/TfPAwWNM2HZZU907sx
aXpZ5EjEi4aQDseUSdSwmFjopH8uWDd1zktXirhvRjDWLE+DiV4trDr+O25xZTaAQKSFNeo0/u42
S/pNIDIdckq3R/xI78xT/uvpSxoafTlQe4u3TiY+Ob01Awv1AhQEKt7cS0BicjQDzashdwrfSo9x
2dMsT1GQSiiAS/b+3SxoKFQM4TN/ofSH5k2LzKKv9ZFAF6SA0HTyba9HJSp+hyPjNhH2edRoz2e/
UlPZdA6ANusXw9QCxuS4l8HAWrgYm0cJmbFPkCWscPxZ6g+r7808y4ErYlme3F9D+PTxizSp6Ol4
cwhgRYaYM3x4H9yqfC4HksJa/jqGHL4nZlfPMPNsZUov4aTuRa8RFSbg/kJwGfVt3bAfJm1CJMR0
Pp91/DuYnhBPKX3YM9a/UCqH9Bj4aP4chw0n+ZcWG7wPRg5K5skukqZDHNNlmEzjFdY06Af7LOop
Ae3fJ31ZruUZhSDIXFeml8YMzx1rG4oOtWjVL6PHDGwJDVV+ya0JUcxn5I8BJuxxlOytRotMp5eS
3AeTItCo5DbXl6jH2BduIT3txkDOwdJfOVfQ6IMlVPIfU4xW77aXHAtLbXPJHek6exOsVHuUguso
Ub+GR6OVfKC17qtyWoCC0FXEOI/gqxLFLEO6IdxcffkDxcwU53bj7+fQUr27CCk2g5nogUnVXInv
TXwkGgzNCXtwwGu5NXWZCQS3iYGJ3S0bGy59Qzje/WWvc2qGkEFyDuPr8QI6AEC73+sNavNSYglO
6SbDlsU0bNdTzYaXLSfC4qFUOzkHjE+yBohCgqa+AAg94jgzMJmaduWhIvGtEDRdxfiCXx83Q+wG
dDrUFbnh/CZtcdJKOT7yQn1wf+1R1T1cAn297HjW0LDcorsZpdflBwrXf6trwAdr7D3dUt1WIb2h
RZApOLKvmshHUO3oz/YPxIdBKQ/YFl1rd6ajR6spfbQ1Y7audxglME7Zyzew7ZEj5l8MrkYLJFm+
xVUC+ODr5MBc+PEHmTnZkMe1duj13wqcBlRo6x2Oo0uD6rT/MISu0u1ghZrD8a+72397GPMsaBqM
Ye533yTVMRzLdG/zYqOzzXi/vyoE6IPjw5n8k/7dW99/kCc3xidSxSDunM8rPryodHeRItgHZ4Y9
UUos51EwKO2JIUgDNd4rNbm719HPeapBZdChK+MKxr24b0XsZF7tnCiLN/pXzHxI3eINmFQgn62f
rVhlHLZnbjSRvJrZ3EACu3YXHYz9i3yf4I+J/7M1cOgngxo4OGtsm/i82A8UuErAmOYQciD3MQc/
aoYJrh5OZ5BjiGQRUVRJBbNBzZamAXM6vWHXJONt7gOXpfkz9GPXA9b6KKjAx0GVj7zMa66CiJSl
GuuaIWezvJFbuJohmwQcmg4IMnSk2c9zdllTAMHtGE/UdWhAkTD+VOxvtZ+mwqynh3zat2nGJFbY
rh2fuT4MdYu6NlKvHUAwlR+1AgKAp6dzV9vS0jzjwjr8+oZgBQDcUQXcIQaaBt5wiVKo+7p/ruOD
R58lQVRM9m59Hx9urrf8eJD1vnls6ditXrORCLmxAhfKakoCkPD0ByHgvIoO59SftHppIHIAZS3Q
TgLOji8cNpzIH3TKLWIMYpZCLr8cbtBtgAMAAZm7URzgrUWMaRyssKDN6/cxZL3Hk9MQLEaixM7k
/DRx7EFJwurvnCuLXCJLcZ8q/WGnZLhPfKKfMOyA0XMiaYmmru0VMnmiWgcjcKV2DsYqD+nabos+
VdEEoGrEEOozk41P3+PJ5BTyQ4WUKe5EKANGKuuu/GSHJFuLj5gqnoBOXJ3M4jxSAzuJpEB9g+eg
PmTw9V0erXVocdAGTxfCwL0VML3XSLQPudB5JlvGZuvbYZ7aEbVdLh1nsT9AVOcmvIUvHjk6ZpWh
r+gjUZ5EKEItTFHiJ7QgR6wphfLfCbE+d/CSctQs5e05hRmzdiMXfWB7y/EgXUiR1n/hTG2vdpTW
X1LW6AokZRSCA9wU0ALDrx4GzCYNU3RSTWiimWKeJv0U+M0EzrFhirPwlAIzG0GDH3ev6IF7xCJ6
eJP5wYQBe8HPIJ7SlRFHfhTqgev1gZY9Ne6YwONB+qnQX/YN5n1z6nR1danUOtPnrmSFfzS3qXo/
aFUMIrnQgF9/0I6H0SAwlYJC1e1v92EjWF+qQ1lYOwvtZQmIpv1YuBZReS6km+Gb4S8YH+Zuqo9I
FnOztPOuMwzpJ7zd3IZmVd4Ar80EjD5qPi5qjwp8goUALuQ+shoJXIkalYm/ba+EeVC+Bpob3nBD
+1NK+/sMDb6X+U8hKa+OIt17WSoGZ9e6BkGZNJUKexvdabHJRcFrErMYAauk6bSnIBiW22pk0+bh
0mHdHv3fBHFjUzRRaU8cef18hY5AeDkfqpdpzP8juI7eyoepO13GslUVslaLmS6AKZOu19Qy64u7
Lz2d6DGAGaFoUGJi5ES5nn0P5ejn+QRlQFoLtuAcdinOf1hYxqeCbT+C6Mp6w280rBrhFfQP3oP/
VXGT3cSj3BUQarMylVBVQDECvMOR+z4+RboRZvOfxkSqcmDSsq1mHMMIA+TD9+pv7fWA+6YL8TIO
7ZCmLYvrWbeQ99+YUYM32kNE2MH4x2NYCnu1zMVQqMpA2BT0L1ib7bi5L+/DCFsclmsoKjRPY3G0
LH3aTIO0aIE5vjX9+a8IaOApn0bD7ippilMIqxYA2sxe4vbXL4yaNQbtp7Sl1IYfrij4cr+nP1x+
m8NiuPb0ESmBrmYJkjvZk1sA6qg0PZudgdg96Ojks/DNsdAABr/roDCEhldhfjWTJjQel0EmmKim
zHV+Bsvj4VsIHYt8zJ7n9beHzTbXpi9bVIDePz16CWAPspl1rGyJb9X4FYW64e1WYPEWaaTfn3/k
Pz3tJbyxt98Wci5TXpuZQv7IPZqMJ+U2ascdyvY9kI1LjAOG+r9PAvMUll7p+mCfSqFpzxCkwUGq
yudf92yJUXJ4c8xtuxy7Dwc0m8ZXQlBv3jq/lnBvt5idWB8qk9OWSbfRXo+acErAGd4YeHCVyxqr
wbN9FQvCegeIrLx7ncSeWMwbxP6yshh1FlfcJQJwR3vW1zkk4i7ClYUrNfpPUEpJSXF+PsBMCmm1
PxIUxn+jAP21QI7jST9zTxv3Gb0OLr6NWd2AlNntYviuXCm5ToVI9+9X4guFD4GNoflxGS0YP7Aq
s/S84mqFqQX+5/MUWeiY6VzDGgxsfjd8y1+WMNk6O2ThsDz4mEeoFbFB8/P8rrBTDfqp5E7URpan
LSMnT2GK1iPZyE2nBdQh2Gf9mc+nOIRpY3W05hJ2lSWEcKUpQpXZkI0J9KwJCC4sz4ci9XOEUAct
k6axW8Q/zLR+NwvWv/xVDUuk9UafcvlAMMYCg7Cl6rSt01aliDTojkMY33/tivkruhXivB8HvJRe
VW0ePseL32KIE0obrdYvTv7/PrRB6stlocFllrApEU1JzYHzBC0ZbQbBjAAhgdbYf3KOJFjvbKDD
xp9gOlWFsqul1A3TX27I415eu63XE27hG8XwslPxAN4YytzaQER2vGO89wby1WLH4fNSEwbPOSPh
WPa4jb7nAq1ChNAIWrYpNzkI8IxCVb3M2I1P9sWokKT4tv1RmszBON93V1qcmbqwdYGI6pVV1sH5
/TdqTGgJSlptpTw64aE8r+DgUN3c3VSfwf2CHnAAtXJTSPfIF3zPsguKtKJ555zfYXWcMXD0G2f+
fP+rqkHMEDIP
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
